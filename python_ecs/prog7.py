
#classes (OOPS)
class Test:
    def fun(self):
        print("Hello")
        
obj=Test()
obj.fun()



#plotting using matplotlib
plt.plot([1,2,3,4,5,6,7,8,9])
plt.ylabel("rnd_numy")
plt.xlabel("rnd_numx")
plt.show()


plt.plot([1,2,3,4,5,6,7,8,9],[0.5,0.10,0.15,0.20,0.25,0.30,0.40,0.45,0.9],'ro', s=100)
plt.ylabel("rnd_numy")
plt.xlabel("rnd_numx")
plt.show()

plt.figure(1)
#number 2 in fixed,1 for figure number, 1 for subplot number
plt.subplot(211)
plt.scatter([1,2,3,4,5,6,7,8,9],[0.5,0.10,0.15,0.20,0.25,0.30,0.40,0.45,0.9], s=75)
plt.plot([1,2,3,4,5,6,7,8,9],[0.5,0.10,0.15,0.20,0.25,0.30,0.40,0.45,0.9],'k')
plt.ylabel("rnd_numy")
plt.xlabel("rnd_numx")
plt.subplot(212)
plt.scatter([1,2,3,4,5,6,7,8,9],[0.5,0.10,0.15,0.20,0.25,0.30,0.40,0.45,0.9], s=75)
plt.plot([1,2,3,4,5,6,7,8,9],[0.5,0.10,0.15,0.20,0.25,0.30,0.40,0.45,0.9],'r')
plt.ylabel("rnd_numy")
plt.xlabel("rnd_numx")
plt.show()

#numpy
import numpy as np
data=np.array([[1,2,3],[4,5,6]])
dat1=np.array([1,2,3], ndmin=3)
#np.asarray - to convert lists to array

#ndmin give the minimum dimension
print(data.shape)
b=data.reshape(3,2) 
#give dimensions (in reshape) depending on the number of elements in the input matrix
a=[1,2,3,4,5,6,7,8,9,10]
c=np.arange(10)
b=slice(2,7,2)
a[b]
a[2:7:2]

#using slice or [x:y:z] gives a numpy array
#fetching the value using [x] or [x:y] gives a non numpy array

a=np.array([[1,2,3],[4,5,6],[7,8,9]])
b=a[:,1:2]
a.ndim #finds the dimension of the array

#pandas
dict={'a':[1,2,3,4],'b':[566],'c':['g','o','o','g','l','e']}
s=pd.Series(dict,index=['a','b','c','d'])
print(s)
e=[[1,2,3],[4,5,6],[7,8,9]]
d=pd.DataFrame(a)
f=pd.DataFrame(e)


df.drop(df.iloc([NaN]))





